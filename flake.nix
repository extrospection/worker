{
  description = "Let some website figure out how you really feel";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    {
      overlay = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlay
        (final: prev: {
          extrospection-worker = prev.poetry2nix.mkPoetryApplication {
            projectDir = ./.;
          };
        })
      ];
    } // (flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in
      {
        apps = {
          extrospection-worker = pkgs.extrospection-worker;
        };

        defaultApp = pkgs.extrospection-worker;

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            poetry

            (python39.withPackages(pythonPackages: with pythonPackages; [
              python-lsp-server
            ]))
          ];
        };
      }));
}
